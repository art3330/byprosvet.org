FROM node:18-alpine

COPY . /app
WORKDIR /app

EXPOSE 3000

CMD ["sh", "-c", "yarn start"]

