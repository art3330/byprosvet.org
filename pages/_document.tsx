import { Html, Head, Main, NextScript } from "next/document";
import { Global, css } from "@emotion/react";

export default function Document() {
  return (
    <Html>
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap"
          rel="stylesheet"
        />
        <Global
          styles={css`
            body {
              margin: 0;
              padding: 0;
              font-family: "Montserrat", sans-serif;
            }
          `}
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
