import type { NextApiRequest, NextApiResponse } from "next";
import { getProductsList } from "../../shop/shopify-client";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (process.env.API_PROXY_URL) {
    try {
      const response = await fetchFromProxy();
      return res.status(200).json(response);
    } catch (e) {
      return res.status(500).json({ error: "Proxy errored" });
    }
  }
  const response = await getProductsList();
  res.status(200).json({ products: response });
}

async function fetchFromProxy() {
  const proxyResponse = await fetch(
    `${process.env.API_PROXY_URL}/api/products`
  );
  return proxyResponse.json();
}
