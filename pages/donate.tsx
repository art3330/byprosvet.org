import Head from "next/head";
import styled from "@emotion/styled";
import { Donations } from "../shop/donate"

export default function Donate() {
  return (
    <Body>
      <Head>
        <title>ByProsvet</title>
      </Head>

      <Main><Donations /></Main>
    </Body>
  );
}

const Body = styled.div`
  font-family: CustomSerif, Georgia, Cambria, "Times New Roman", serif;
  line-height: 1.58;
  a {
    color: inherit;
    border-bottom: 0.1em solid rgba(0, 0, 0, 0.7);
    text-decoration: none;
  }
`;

const Main = styled.main`
  padding: 0.5rem 1rem;
  h1 {
    text-align: center;
  }
  div {
    margin: 0 auto;
    max-width: 700px;
  }
  ul {
    padding-left: 0.5rem;
  }
  @media (min-width: 480px) {
    padding: 1rem 5rem;
    ul {
      padding-left: 1rem;
    }
  }
`;

const WalletAddress = styled.span`
  font-family: monospace;
  background-color: #ddd;
  font-size: 0.8rem;
  @media (min-width: 480px) {
    font-size: 0.95rem;
  }
`;

