import Head from "next/head";
import { Header } from "../shop/navigation";
import { Promotion } from "../shop/promotion";
import { Samvydat } from "../shop/samvydat";

export default function Shop() {
  return (
    <div>
      <Head>
        <title>ByProsvet - самвыдат Беларусі</title>
      </Head>
      <Header />
      <Promotion />
      <Samvydat />
    </div>
  );
}
