// eslint-disable @next/next/no-before-interactive-script-outside-document
import Script from "next/script";
import { useEffect, useCallback } from "react";

export default function TgApp() {
  const showMainButton = useCallback(() => {
    window.Telegram?.WebApp.MainButton.show();
    window.Telegram?.WebApp.MainButton.onClick(() => {
      window.Telegram?.WebApp.close();
    });
  }, []);
  useEffect(() => {
    setTimeout(() => {
      window.Telegram?.WebApp?.MainButton.showProgress(true);
    }, 3000);
    window.Telegram?.WebApp?.ready();
  }, []);
  return (
    <main>
      <Script
        src="https://telegram.org/js/telegram-web-app.js"
        strategy="beforeInteractive"
      ></Script>
      <div>ByProsvet ready for button?</div>
      <button onClick={showMainButton}>Open button</button>
    </main>
  );
}
