import styled from "@emotion/styled";
import { PropsWithChildren } from "react";
import { colors } from "../uikit";
import { ShoppingCart } from "../uikit/icons";

export const AddToCartButton = (
  props: PropsWithChildren<{ onClick: () => void }>
) => (
  <Button onClick={props.onClick}>
    <ShoppingCart width="1.5rem" height="1.5rem" />
    <span>{props.children}</span>
  </Button>
);

export const Button = styled.button`
  display: flex;
  flex-direction: row;
  color: ${colors.fontHighlight};
  background-color: ${colors.backgroundSecondary};
  border-radius: 4px;
  border: 0;
  padding: 0;
  margin: 0.75rem 0;
  font-size: 1rem;
  cursor: pointer;
  svg {
    display: inline-block;
    margin: 0.75rem 0.5rem 0.75rem 1rem;
  }
  span {
    display: inline-block;
    margin: 14px 1rem 14px 0;
  }
`;
