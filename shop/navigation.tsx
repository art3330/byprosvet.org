import styled from "@emotion/styled";
import { colors } from "../uikit";
import logoUrl from "../uikit/logo.svg";
import Image from "next/image";

export const Header = () => (
  <TopBar>
    <Logo>
      <Image src={logoUrl} alt="byprosvet logo" />
    </Logo>
    <Navigation>
      <ul>
        <li>
          <SupportLink href="/donate">Падтрымаць нас</SupportLink>
        </li>
      </ul>
    </Navigation>
  </TopBar>
);

const TopBar = styled.header`
  display: flex;
  height: 80px;
  justify-content: space-between;
`;

const ToggleMenuButton = styled.button`
  border: 0;
  padding: 0;
  margin: 0 0 0 24px;
  background: none;
  color: #00aeef;
  @media (min-width: 640px) {
    display: none;
  }
`;

const Logo = styled.div`
  width: 200px;
  padding: 1rem 2vw;
`;

const Navigation = styled.nav`
  font-size: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  ul {
    list-style: none;
    display: none;
    flex-direction: row;
    @media (min-width: 640px) {
      display: flex;
    }
  }
`;

const MenuLink = styled.a`
  margin: 0 0.5vw;
  @media (min-width: 55rem) {
    margin: 0 1vw;
  }
  text-decoration: none;
  color: ${colors.fontMain};
  &:hover {
    color: ${colors.backgroundSecondary};
  }
`;

const SupportLink = styled.a`
  padding: 0.625rem 0.75rem;
  margin: 0 0.5rem;
  @media (min-width: 55rem) {
    padding: 0.625rem 1.5rem;
    margin: 0 0.5rem 0 1.5rem;
  }
  background-color: ${colors.backgroundSecondary};
  border-radius: 2px;
  text-decoration: none;
  color: ${colors.fontHighlight};
`;

const Cart = styled.a`
  display: inline-block;
  width: 48px;
  margin: 0 0.25rem;
  @media (min-width: 55rem) {
    margin: 0 1.25rem;
  }
  color: ${colors.fontMain};
  &:hover {
    color: ${colors.fontSecondary};
  }
`;
