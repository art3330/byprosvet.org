import styled from "@emotion/styled";
import { Cell18 } from './promotion/cell18'
import { CalendarKozhnyDzen2023 } from './promotion/CalendarKozhnyDzen'

export const Promotion = () => (
  <PromotionContainer>
    <CalendarKozhnyDzen2023 />
    <Cell18 />
  </PromotionContainer>
);

const PromotionContainer = styled.div`
`;
