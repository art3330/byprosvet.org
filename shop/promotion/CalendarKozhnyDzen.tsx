import { useCallback } from "react";
import calendarImage from "../img/kozhny-dzen-2023.png";
import {
  BookDescription,
  Slide,
  BookPreview,
  BuyContainer,
  BuyNowButton,
  ProductDetails,
} from "../styled";
import { InstagramIcon } from "../../uikit/icons";
import { OrderQuantityControl, usePromotionPrice } from "./common";
import Image from "next/image"

const buyIt = (quantity: number) => {
  window.location.href = `https://cart.byprosvet.org/cart/42769598120089:${quantity}`;
};

const CalendarKozhnyDzen2023Price = 140;

export const CalendarKozhnyDzen2023 = () => {
  const { price, quantity, setQuantity } = usePromotionPrice(
    CalendarKozhnyDzen2023Price
  );
  const buy = useCallback(() => buyIt(quantity), [quantity]);

  return (
    <Slide>
      <BookPreview>
        <Image src={calendarImage} alt="Каляндар Кожны Дзень 2023" />
      </BookPreview>
      <BookDescription>
        <h1>Кожны дзень 2023</h1>
        <h2>
          <a href="https://instagram.com/hakuna_na_ta_ta" target="_blank">
            <span>hakuna_na_ta_ta</span>
            <InstagramIcon />
          </a>
          <a href="https://instagram.com/emilka_plater" target="_blank">
            <span>emilka_plater</span>
            <InstagramIcon />
          </a>
          Наста і Адэля Бяловіч
        </h2>
        <p>
          Назва, пад якой хаваюцца 365 цікавінак беларускай гісторыі. Штрыхі да
          партрэтаў людзей, якія пакінулі нам шыкоўную духоўную спадчыну.
          Падзеі, якія змянілі беларусаў.
        </p>
      </BookDescription>
      <BuyContainer>
        <ProductDetails>
          <p>Месца выдання: Польшча</p>
          <p>Старонак: 367.</p>
          <p>Год выдання: 2022</p>
        </ProductDetails>
        <OrderQuantityControl onChange={setQuantity} count={quantity} />
        <h2>
          {CalendarKozhnyDzen2023Price}zl{" "}
          {quantity > 1 ? `* ${quantity} = ${price}zł` : ""}
        </h2>
        <BuyNowButton onClick={buy}>Купіць каляндар</BuyNowButton>
      </BuyContainer>
    </Slide>
  );
};
