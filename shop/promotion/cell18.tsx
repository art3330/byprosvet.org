import { useCallback } from 'react'
import cell18Preview from '../img/kamera18.png'
import {
  BookDescription,
  Slide,
  BookPreview,
  BuyContainer,
  AddToCartButton,
  ProductDetails,
} from '../styled'
import { OrderQuantityControl, usePromotionPrice } from './common'
import Image from "next/image"

const buyIt = (quantity: number) => {
  window.location.href = `https://cart.byprosvet.org/cart/42296067719321:${quantity}`
}

const Cell18Price = 80;

export const Cell18 = () => {
  const { price, quantity, setQuantity } = usePromotionPrice(Cell18Price);
  const buy = useCallback(() => buyIt(quantity), [quantity])

  return (
    <Slide>
      <BookPreview>
        <Image src={cell18Preview} alt="Камера №18" />
      </BookPreview>
      <BookDescription>
        <h1>Камера #18</h1>
        <h2>Валерий Панюшкин, Варвара Панюшкина</h2>
        <p>
          В одиннадцатиметровой камере #18 минской следственной тюрьмы на улице
          Окрестина почти неделю без еды, воды, лекарств, одежды, средств
          гигиены и почти без воздуха были заперты 36 женщин. После освобождения
          нам удалось поговорить с 11 из них. О них наша история.
        </p>
      </BookDescription>
      <BuyContainer>
        <ProductDetails>
          <p>Место издания: Варшава</p>
          <p>Год издания: 2022</p>
          <p>Страниц: 112</p>
          <p>ISBN: 978-83-963661-3-9</p>
        </ProductDetails>
        <OrderQuantityControl onChange={setQuantity} count={quantity}/>
        <h2>{price}zł</h2>
        <AddToCartButton onClick={buy}>Купить книгу</AddToCartButton>
      </BuyContainer>
    </Slide>
  )
}
