import { FC, useState } from 'react'
import { OrderQuantityWrap, PlusMinusButton, OrderQuantity } from '../styled'
import { AddIcon, RemoveIcon } from '../../uikit/icons'

export interface OrderCountControlProps {
  count: number;
  onChange: (value: number) => void;
}

export const OrderQuantityControl: FC<OrderCountControlProps> = ({
  count,
  onChange,
}) => {
  const increment = () => onChange(count + 1)
  const decrement = () => {
    if (count === 1) {
      return
    }

    onChange(count - 1)
  }

  return (
    <OrderQuantityWrap>
      <PlusMinusButton onClick={increment}>
        <AddIcon/>
      </PlusMinusButton>
      <OrderQuantity>{count}</OrderQuantity>
      <PlusMinusButton onClick={decrement}>
        <RemoveIcon/>
      </PlusMinusButton>
    </OrderQuantityWrap>
  )
}

export const usePromotionPrice = (originalPrice: number) => {
  const [quantity, setQuantity] = useState(1)

  const price = originalPrice * quantity

  return { price, setQuantity, quantity }
}
