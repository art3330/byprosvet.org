import styled from "@emotion/styled";
import { fontSecondary } from "../uikit/colors";
import { ArrowForward } from "../uikit/icons";
import Image from "next/image";
import belCasPreview from "./img/bel-cas-3.png";
import golasBelPreview from "./img/golas-bel-3.png";
import volunteerHandUrl from "./img/volunteer-hand.png";

export const Samvydat = () => {
  return (
    <Container>
      <PapersContainer>
        <h1>Штотыднёвы самвыдат</h1>
        <p>
          Можна пакідаць у паштовых скрынях суседзяў, на падваконнях пад'ездаў,
          на прыпынках вашых горада, мястэчка ці вёскі; дзяліцца з калегамі,
          знаёмымі, роднымі, сябрамі
        </p>
        <PapersList>
          <PaperContainer>
            <PaperPreview>
              <Image src={golasBelPreview} alt="Голос Беларуси" />
            </PaperPreview>
            <h2>Голос Беларуси</h2>
            <p>23 декабря 2022, №3</p>
            <ul>
              <li>
                Беларусские медики о зарплатах, нагрузках, отношении пациентов и
                ситуации в стране,
              </li>
              <li>экономика Беларуси по-прежнему работает в минус</li>
              <li>эксперты о встрече Лукашенко и Путина,</li>
              <li>как в Беларуси увольняли за политику в 2022 году,</li>
              <li>
                Светлана Тихановская прокомментировала заочные суды над
                беларусами,
              </li>
            </ul>
          </PaperContainer>
          <DownloadButton link="https://t.me/by_prosvet/2534" />
          <PaperContainer>
            <PaperPreview>
              <Image src={belCasPreview} alt="Беларускі Час" />
            </PaperPreview>
            <h2>Беларускi Час</h2>
            <p>24 снежня 2022, №3</p>
            <ul>
              <li>
                Визит Путина в Беларусь - только об одном: он не оставляет
                надежды втянуть Лукашенко и его армию в прямое участие в
                оккупационной войне.
              </li>
              <li>
                бщее количество военных организаций по воспитанию детей, не
                считая клубов и лагерей МВД, в Беларуси уже несколько сотен.
              </li>
              <li>
                Лукашенко превратил Беларусь в кормушку для себя и своей семьи.
                Факты.
              </li>
            </ul>
          </PaperContainer>
          <DownloadButton link="https://t.me/by_prosvet/2539" />
        </PapersList>
      </PapersContainer>
      <FlyersContainer>
        <h1>Валанцёрскі рух</h1>
        <Link href="https://t.me/by_prosvet/2137" target="_blank">
          <span>Далучыцца</span>
          <ArrowForward />
        </Link>
        <HandContainer>
          <Image src={volunteerHandUrl} alt="Далучайся" />
        </HandContainer>
      </FlyersContainer>
    </Container>
  );
};
const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem 5vw;
  @media (min-width: 1201px) {
    flex-direction: row;
  }
  justify-content: center;
  background-color: #f8f7f5;
`;

const PapersContainer = styled.div`
  flex-grow: 2;
  h1 {
    text-transform: uppercase;
  }
`;

const PapersList = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-template-areas:
    "P1"
    "M1"
    "P2"
    "M2";
  @media (min-width: 641px) {
    grid-template-columns: 50% 50%;
    grid-template-areas:
      "P1 P2"
      "M1 M2";
  }
  justify-content: space-between;
`;
const PaperContainer = styled.div`
  &:first-of-type {
    grid-area: P1;
  }
  &:nth-of-type(2) {
    grid-area: P2;
  }
  margin: 0 1rem 0 0;
  ul {
    padding: 0 0 0 20px;
  }
  li::marker {
    font-size: 0.5rem;
  }
  h2 {
    font-size: 1.5rem;
    font-weight: 600;
    line-height: 29px;
    margin-bottom: 2px;
  }
  p {
    font-size: 12px;
    line-height: 15px;
    margin-top: 2px;
  }
`;

const DownloadButton = (props: { link: string }) => (
  <Button href={props.link} target="_blank">
    <span>Больш падрабязна</span>
    <ArrowForward />
  </Button>
);
const Button = styled.a`
  &:first-of-type {
    grid-area: M1;
  }
  &:nth-of-type(2) {
    grid-area: M2;
  }
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 0 0 2rem 0;
  color: ${fontSecondary};
  background-color: transparent;
  border-radius: 4px;
  text-decoration: none;
  border: 2px solid ${fontSecondary};
  font-size: 1rem;
  @media (min-width: 641px) {
    width: 250px;
  }
  @media (min-width: 1201px) {
    margin: 0px;
    width: 300px;
  }
  svg {
    margin: 0.65rem 0 0.5rem 1vw;
    font-size: 16px;
  }
  span {
    margin: 0.5rem 0 0.5rem 0;
    font-size: 16px;
  }
`;
const PaperPreview = styled.div`
  width: 100%;
  height: 50vw;
  overflow: hidden;
  border: 1px solid #e2e2e2;
  img {
    max-width: 100%;
    height: auto;
  }
  @media (min-width: 641px) {
    width: 40vw;
    max-width: 380px;
    height: 240px;
  }
`;

const FlyersContainer = styled.div`
  flex-grow: 1;
  background-color: #00aeef;
  color: #fff;
  text-align: center;
  padding: 2rem 2rem 0 2rem;
  h1 {
    font-size: 40px;
    font-weight: 600;
  }
`;

const Link = styled.a`
  color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 1rem 0;
  width: 100%;
  @media (min-width: 641px) {
    width: 300px;
    margin: 0 auto;
  }
  background-color: transparent;
  border-radius: 4px;
  text-decoration: none;
  border: 2px solid #fff;
  font-size: 1rem;
  svg {
    margin: 0.625rem 0 0.5rem 1vw;
    font-size: 16px;
  }
  span {
    margin: 0.5rem 0 0.5rem;
    font-size: 16px;
  }
`;
const HandContainer = styled.div`
  margin-top: 3rem;
  height: 340px;
  overflow: hidden;
`;
