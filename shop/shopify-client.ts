export async function getProductsList() {
  const graphqlResponse = await fetchFromShopify()
  return graphqlResponse.data.products.edges.map(product => ({
    ...product.node,
    variants: product.node.variants.edges.map(variant => variant.node)
  }))
}

const apiHost = process.env.SHOPIFY_API_HOST
const storefrontAccessToken = process.env.SHOPIFY_STOREFRONT_ACCESS_TOKEN || ""

type Response = {
  data: {
    products: {
      edges: Array<{
        node: {
          id: string
          title: string
          descriptionHtml: string
          featuredImage: {
            url: string
            height: number
            width: number
          }
          priceRange: {
            minVariantPrice: {
              amount: string
              currencyCode: string
            }
            maxVariantPrice: {
              amount: string
              currencyCode: string
            }
          }
          variants: {
            edges: Array<{
              node: {
                id: string
              }
            }>
          }
        }
      }>
    }
  }
}
async function fetchFromShopify(): Promise<Response> {
  const response = await fetch(
    `https://${apiHost}/api/2022-10/graphql.json`,
    {
      method: "post",
      headers: {
        "X-Shopify-Storefront-Access-Token": storefrontAccessToken,
        "Content-Type": "application/graphql",
      },
      body: `{
  products(first:10) {
    edges {
      node {
        id
        title
        descriptionHtml
        featuredImage {
          url
          height
          width
        }
        priceRange {
          maxVariantPrice {
            amount
            currencyCode
          }
          minVariantPrice {
            amount
            currencyCode
          }
        }
        variants(first: 10) {
          edges {
            node {
              id
            }
          }
        }
      }
    }
  }
}`,
    }
  );
  return response.json();
}
