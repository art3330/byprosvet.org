import styled from "@emotion/styled";
import { PropsWithChildren } from "react";
import { ShoppingCart, AccessTime } from "../uikit/icons";
import { colors } from "../uikit";

export const Slide = styled.div`
  margin: 0 5vw;
  display: grid;
  grid-template-areas:
    "preview preview"
    "description description"
    "buy buy";
  grid-template-columns: 35% 65%;
  @media (min-width: 641px) {
    grid-template-areas:
      "preview description"
      "preview buy"
      "preview .";
    padding: 1rem 0;
  }

  p {
    margin: 0.5rem 0;
    font-size: 14px;
  }
`;
export const BookPreview = styled.div`
  grid-area: preview;
  img {
    max-width: 100%;
    height: auto;
  }
  @media (min-width: 641px) {
    padding: 1rem 5vw 0 0;
  }
`;

export const BookDescription = styled.div`
  grid-area: description;
  padding: 0;
  line-height: 24px;
  @media (min-width: 641px) {
    padding: 1rem 0vw 0 0vw;
  }

  h1 {
    margin: 0;
    font-size: 2rem;
    @media (min-width: 641px) {
      font-size: 3rem;
      margin: 1rem 0;
    }
    font-weight: 700;
    line-height: 3rem;
  }

  h2 {
    display: flex;
    flex-direction: column;
    @media (min-width: 641px) {
      display: block;
    }
    line-height: 24px;
    font-size: 16px;
    font-weight: 600;
    a {
      display: inline-block;
      color: #000;
      text-decoration: none;
      margin-right: 24px;
      svg {
        margin-bottom: -6px;
        margin-left: 8px;
      }
    }
  }
  p {
    color: #373737;
    font-size: 16px;
  }
`;

export const ProductDetails = styled.div`
  color: #373737;
  line-height: 24px;
  padding-bottom: 24px;

  p {
    margin: 0;
    font-size: 14px;
  }
`;

export const BuyContainer = styled.div`
  grid-area: buy;
  align-self: start;
  display: block;
  h2 {
    font-size: 20px;
    s {
      color: rgb(188, 187, 185);
      margin-right: 16px;
    }
  }
`;

export const AddToCartButton = (
  props: PropsWithChildren<{ onClick: () => void }>
) => (
  <Button onClick={props.onClick}>
    <ShoppingCart />
    <span>{props.children}</span>
  </Button>
);

export const BuyNowButton = (
  props: PropsWithChildren<{ onClick: () => void }>
) => (
  <Button onClick={props.onClick}>
    <ShoppingCart />
    <span>{props.children}</span>
  </Button>
);

export const PreOrderButton = (
  props: PropsWithChildren<{ onClick: () => void }>
) => (
  <Button onClick={props.onClick}>
    <AccessTime />
    <span>{props.children}</span>
  </Button>
);

export const Button = styled.button`
  display: flex;
  flex-direction: row;
  color: ${colors.fontHighlight};
  background-color: ${colors.backgroundSecondary};
  border-radius: 4px;
  border: 0;
  padding: 0;
  margin: 0.75rem 0;
  font-size: 1rem;
  cursor: pointer;
  justify-content: center;
  width: 100%;
  @media (min-width: 641px) {
    width: 280px;
  }

  svg {
    display: inline-block;
    margin: 0.5rem 0.5rem 0.5rem 1rem;
  }

  span {
    display: inline-block;
    margin: 0.625rem 1rem 0.5rem 0;
  }
`;

export const OrderQuantityWrap = styled.div`
  height: 48px;
  width: 96px;
  border-radius: 4px;
  padding: 0 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #cecece;
  box-sizing: border-box;
`;

export const PlusMinusButton = styled.button`
  border: none;
  background: none;
  padding: 0;

  > svg {
    color: #6d7076;
    font-size: 18px;
    vertical-align: middle;
  }
`;

export const OrderQuantity = styled.div`
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  color: #0b111a;
  text-align: center;
  min-width: 0;
  text-overflow: ellipsis;
  overflow: hidden;
`;
