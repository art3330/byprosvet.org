interface Window {
  Telegram?: {
    WebApp: {
      close: () => void;
      ready: () => void;
      MainButton: TelegramMainButton;
    };
  };
}

interface TelegramMainButton {
  text: string;
  color: string;
  textColor: string;
  show: () => void;
  hide: () => void;
  enable: () => void;
  disable: () => void;
  showProgress: (leaveActive: boolean = false) => void;
  hideProgress: () => void;
  setParams: (params: {
    text?: string;
    color?: string;
    text_color?: string;
    is_active?: boolean;
    is_visible?: boolean;
  }) => void;
  setText: (text: string) => void;
  onClick: (callback: () => void) => void;
}
