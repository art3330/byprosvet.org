import AccessTime from "@mui/icons-material/AccessTime";
import ShoppingCart from "@mui/icons-material/ShoppingCart";
import ArrowForward from "@mui/icons-material/ArrowForward";
import DownloadIcon from "@mui/icons-material/Download";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import MenuIcon from "@mui/icons-material/Menu";
import InstagramIcon from '@mui/icons-material/Instagram';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';

export {
  AccessTime,
  ShoppingCart,
  ArrowForward,
  DownloadIcon,
  CreditCardIcon,
  MenuIcon,
  InstagramIcon,
  AddIcon,
  RemoveIcon,
};
